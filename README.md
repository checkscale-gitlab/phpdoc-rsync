# naala89/phpdoc-rsync

This image generates a static PHPDOC site with the capability to  upload to a
remote location.

## Getting Started

These instructions will cover usage information and for the docker container

### Prerequisites

In order to run this container you'll need docker installed.

* [Windows][docker_windows]
* [OS X][docker_osx]
* [Linux][docker_linux]

### Usage

#### Environment Variables

None

#### Volumes

* ```/app``` - Source markdown to be parsed by Bookdown.

#### Useful File Locations

* ```/composer/vendor/bin/bookdown``` - Location of the bookdown binary.

#### Example

Run interactively within the container.

```shell
docker run -it --rm -v .:/data naala89/apiopenstudio-bookdown:latest /bin/bash
$ /composer/vendor/bin/bookdown includes/wiki/bookdown.json /app/bookdown.json
$ rsync -rvI --delete --exclude=".*" "/site/" "user@my_site:/var/www/my_site"
$ ssh "user@my_site" "sudo chown -R www-data:www-data /var/www/my_site/*"
```

Run Bookdown within the container (no rsync)````

```shell
docker run --rm -v .:/app naala89/apiopenstudio-bookdown:latest
```````

## Find Us

* [GitLab][gitlab_apiopenstudio]
* [GitHub][github_naala89]
* [www.apiopenstudio.com][web_apiopenstudio]

## Versioning

The unstable tags are tagged with the Gitlab Pipeline ID.

Version tags (i.e. ```phpdoc-rsync:3.1```) references the
[phpdoc/phpdoc][phpdoc_phpdoc] version.

The ```latest``` tag is master branch, his will be based on the most recent tag.

## Authors

* **John Avery** - [John - GitLab][gitlab_john]

## License

This project is licensed under the MIT License - see the
[LICENSE.md][license] file for details.

## Acknowledgments

Many thanks to:

* The contributors to [phpDocumentor][phpdocumentor] for a great documenting 
tool
* The contributors to [phpdoc/phpdoc][phpdoc_phpdoc] for a great docker image.

[docker_windows]: https://docs.docker.com/windows/started
[docker_osx]: https://docs.docker.com/mac/started/
[docker_linux]: https://docs.docker.com/linux/started/
[gitlab_apiopenstudio]: https://gitlab.com/apiopenstudio
[github_naala89]: https://github.com/naala89
[gitlab_john]: https://gitlab.com/john89
[web_apiopenstudio]: https://www.apiopenstudio.com
[phpdoc_phpdoc]: https://hub.docker.com/r/phpdoc/phpdoc
[phpdocumentor]: https://github.com/phpDocumentor/phpDocumentor
[license]: https://gitlab.com/apiopenstudio/docker_images/phpdoc/-/blob/master/LICENSE.md
